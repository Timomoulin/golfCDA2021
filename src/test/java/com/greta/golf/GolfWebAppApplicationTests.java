package com.greta.golf;

import com.greta.golf.model.Golf;
import com.greta.golf.model.Role;
import com.greta.golf.model.User;
import com.greta.golf.service.GolfService;
import com.greta.golf.service.JpaUserService;
import com.greta.golf.service.RoleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
@SpringBootTest
class GolfWebAppApplicationTests {
    @Autowired
GolfService golfService;
    @Autowired
    RoleService roleService;
    @Autowired
    JpaUserService jpaUserService;

    @Test
    void contextLoads() {
    }

    @Test
    @Transactional
    void testAddGolf()
    {
      /*  List<Golf> lesGolfT0=golfService.getAllGolf();
        int nbrGolfsT0=lesGolfT0.size();
        Golf unGolfTest= golfService.addGolf("TestGolf","123");
        List<Golf> lesGolfT1=golfService.getAllGolf();
        int nbrGolfsT1=lesGolfT1.size();
        assertThat(nbrGolfsT1).isEqualTo(nbrGolfsT0+1);
        Golf verifGolf=golfService.getGolfById(unGolfTest.getId());
        assertThat(verifGolf.getName()).isEqualTo("TestGolf");
        assertThat(verifGolf.getCoorGPS()).isEqualTo("123");*/
    }

    @Test
    @Transactional
    void testUpdateGolf()
    {
        Golf unGolfT0=golfService.getGolfById(1L); //Avant modif
        String nomGolfT0=unGolfT0.getName(); // le nom du golf avant modif
        String gpsGolfT0=unGolfT0.getCoorGPS(); // le gps avant modif
        golfService.updateGolf(unGolfT0.getId(),"TrucTestGolf","12525");
        Golf unGolfT1=golfService.getGolfById(1L);//Après modif
        assertThat(unGolfT1.getName()).isNotEqualTo(nomGolfT0);
        assertThat(unGolfT1.getCoorGPS()).isNotEqualTo(gpsGolfT0);

    }
    @Test
    @Transactional
    void testDeleteGolf()
    {
       /*Golf testGolf=golfService.addGolf("TestGolf","123");

        List<Golf> lesGolfT0=golfService.getAllGolf();
        int nbrGolfsT0=lesGolfT0.size();

        Golf unGolfT0=golfService.getGolfById(testGolf.getId());
        golfService.deleteGolf(unGolfT0.getId());

        List<Golf> lesGolfT1=golfService.getAllGolf();
        int nbrGolfsT1=lesGolfT1.size();

        assertThat(nbrGolfsT1).isEqualTo(nbrGolfsT0-1);*/

    }

    @Test
    @Transactional
    void inscrireUtilisateur()
    {
        User utilisateur = new User();
        utilisateur.setNom("Moulin");
        utilisateur.setEmail("tim1omoulin@msn.com");
        utilisateur.setPassword("admin123");
        Role admin=this.roleService.getRoleByName("admin");
        Role arbitre=this.roleService.getRoleByName("arbitre");
        ArrayList<Role> roles=new ArrayList<Role>();
        roles.add(admin);
        roles.add(arbitre);
        utilisateur.setRoles(roles);
        jpaUserService.save(utilisateur);
        System.out.println("mdp = "+utilisateur.getPassword());
        assertThat(utilisateur.getPassword()).isNotEqualTo("admin123");


    }
}
