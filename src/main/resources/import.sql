insert into golf (id, name) values (1, 'Cely');
insert into golf (id, name) values (2, 'Coudray Montceaux');
insert into golf (id, name) values (3, 'Etiolles');
insert into golf (id, name) values (4, 'Ozoir');
insert into golf (id, name) values (5, 'Courson Stade Français');



insert into course (id, name, golf_id) values (1, 'principal', 1);
insert into course (id, name, golf_id) values (2, 'Les marroniers', 2);
insert into course (id, name, golf_id) values (4, 'Parcours du château', 4);
insert into course (id, name, golf_id) values (5, 'Vert-Noir', 5);
insert into course (id, name, golf_id) values (6, 'Vert-Orange', 5);
insert into course (id, name, golf_id) values (7, 'Vert-Lilas', 5);
insert into course (id, name, golf_id) values (8, 'Noir-Orange', 5);
insert into course (id, name, golf_id) values (9, 'Noir-Lilas', 5);
insert into course (id, name, golf_id) values (10, 'Noir-Vert', 5);
insert into course (id, name, golf_id) values (11, 'Lilas-Noir', 5);
insert into course (id, name, golf_id) values (12, 'Lilas-Vert', 5);
insert into course (id, name, golf_id) values (13, 'Lilas-Orange', 5);
insert into course (id, name, golf_id) values (14, 'Orange-Noir', 5);
insert into course (id, name, golf_id) values (15, 'Orange-Lilas', 5);
insert into course (id, name, golf_id) values (16, 'Orange-Vert', 5);



insert into hole (id, name, num, par, course_id) values (1, '', 1, 5, 1);
insert into hole (id, name, num, par, course_id) values (2, '', 2, 4, 1);
insert into hole (id, name, num, par, course_id) values (3, ' ', 3, 4, 1);
insert into hole (id, name, num, par, course_id) values (4, ' ', 4, 4, 1);
insert into hole (id, name, num, par, course_id) values (5, ' ', 5, 3, 1);
insert into hole (id, name, num, par, course_id) values (6, ' ', 6, 5, 1);
insert into hole (id, name, num, par, course_id) values (7, ' ', 7, 3, 1);
insert into hole (id, name, num, par, course_id) values (8, ' ', 8, 4, 1);
insert into hole (id, name, num, par, course_id) values (9, ' ', 9, 4, 1);
insert into hole (id, name, num, par, course_id) values (10, ' ', 10, 5, 1);
insert into hole (id, name, num, par, course_id) values (11, ' ', 11, 4, 1);
insert into hole (id, name, num, par, course_id) values (12, ' ', 12, 3, 1);
insert into hole (id, name, num, par, course_id) values (13, ' ', 13, 4, 1);
insert into hole (id, name, num, par, course_id) values (14, ' ', 14, 4, 1);
insert into hole (id, name, num, par, course_id) values (15, ' ', 15, 5, 1);
insert into hole (id, name, num, par, course_id) values (16, ' ', 16, 4, 1);
insert into hole (id, name, num, par, course_id) values (17, ' ', 17, 3, 1);
insert into hole (id, name, num, par, course_id) values (18, '', 18, 4, 1);

insert into tournament (id,name,course_id) values (1,'test',1);

insert into role (id_role,name) values (1,"admin");
insert into role (id_role,name) values (2,"arbitre");

insert into user (id_user,nom,email,password) values (1,"Moulin","timomoulin@msn.com","$2a$10$I8lCxKnwsVVhyf71OO.nteoKobZ4eIew7ZZRhJncVMuw3lhL8EXSy");


insert into user_role (id_user,id_role) values (1,1);
insert into user_role (id_user,id_role) values (1,2);
