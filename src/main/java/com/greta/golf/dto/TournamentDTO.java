package com.greta.golf.dto;

import com.greta.golf.model.Round;
import com.greta.golf.model.Tournament;
import org.jsoup.Jsoup;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TournamentDTO {
    private long id;
    private String name;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private String comment;
    private List<Round> rounds;
    private long courseId;


    public TournamentDTO() {
    }

    public TournamentDTO(Tournament tournament)
    {
        this.id=tournament.getId();
        this.name=tournament.getName();
        this.date=tournament.getDate();
        this.comment=tournament.getComment();
        this.courseId=tournament.getCourse().getId();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Jsoup.parse(name).text();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    /** public Date stringToDate() throws ParseException {
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.parse(this.date);
    }**/


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = Jsoup.parse(comment).text();
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }


}
