package com.greta.golf.dto.controlForm;

import com.greta.golf.dto.RoundDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class ControlRound implements Validator {
    /**
     * This Validator validates just Person instances
     */
    public boolean supports(Class clazz) {
        return RoundDTO.class.equals(clazz);
    }
@Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "date", "date.empty");
        ValidationUtils.rejectIfEmpty(e, "file", "file.empty");
        ValidationUtils.rejectIfEmpty(e, "attunements", "attunements.empty");
        RoundDTO r = (RoundDTO) obj;

    }


}
