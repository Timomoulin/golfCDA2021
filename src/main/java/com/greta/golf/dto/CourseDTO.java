package com.greta.golf.dto;

import com.greta.golf.model.Golf;
import com.greta.golf.model.Hole;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

public class CourseDTO {
    private long id;
    private String name;
    private long golfId;
    private List<Hole> holes;

    public void addHole(Hole hole)
    {
        this.holes.add(hole);
    }
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getGolfId() {
        return golfId;
    }

    public void setGolfId(long golfId) {
        this.golfId = golfId;
    }

    public List<Hole> getHoles() {
        return holes;
    }

    public void setHoles(List<Hole> holes) {
        this.holes = holes;
    }
}
