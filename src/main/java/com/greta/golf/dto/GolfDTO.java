package com.greta.golf.dto;


import org.jsoup.Jsoup;

import javax.persistence.Column;


public class GolfDTO {

    private long id ;
    private String name;
    private String coorGPS;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = Jsoup.parse(name).text();
    }

    public String getCoorGPS() {
        return coorGPS;
    }

    public void setCoorGPS(String coorGPS) {
        this.coorGPS = Jsoup.parse(coorGPS).text();
    }
}
