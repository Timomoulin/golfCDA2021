package com.greta.golf.dto;

import com.greta.golf.model.Attunement;
import com.greta.golf.model.Tournament;
import com.sun.istack.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

public class RoundDTO {

        private long id;
        @NotNull
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        @FutureOrPresent(message = "Mauvaise date")
        private Date date;

        @NotNull
        private long idTournament;

        @NotNull
        private MultipartFile file;

        @NotNull
        @Size(min=18,max=18,message = "Doit avoir 18 ajustements")
        private List<Attunement> attunements;

        //Todo prop pour fichier csv

    public RoundDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public long getIdTournament() {
        return idTournament;
    }

    public void setIdTournament(long idTournament) {
        this.idTournament = idTournament;
    }

    public List<Attunement> getAttunements() {
        return attunements;
    }

    public void setAttunements(List<Attunement> attunements) {
        this.attunements = attunements;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
