package com.greta.golf.dao;

import com.greta.golf.model.Attunement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttunementRepository extends JpaRepository<Attunement,Long> {
}
