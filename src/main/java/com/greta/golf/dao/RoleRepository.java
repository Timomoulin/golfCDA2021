package com.greta.golf.dao;

import com.greta.golf.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {

    public Role findRoleByName(String nomRole);
}
