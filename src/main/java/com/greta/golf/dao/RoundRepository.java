package com.greta.golf.dao;

import com.greta.golf.model.Round;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoundRepository extends JpaRepository<Round,Long> {
}
