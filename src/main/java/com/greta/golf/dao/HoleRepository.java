package com.greta.golf.dao;

import com.greta.golf.model.Course;
import com.greta.golf.model.Hole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HoleRepository extends JpaRepository<Hole, Long>
{
    public List<Hole> findAllByCourseOrderByNumAsc(Course course);
}
