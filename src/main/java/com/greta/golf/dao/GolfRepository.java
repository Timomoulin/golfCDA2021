package com.greta.golf.dao;

import com.greta.golf.model.Golf;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface GolfRepository extends JpaRepository<Golf, Long>
{
    List<Golf> findGolfByNameOrderByName(String name);
//    List<Golf> findAllGolfOrderByNameAsc();
}
