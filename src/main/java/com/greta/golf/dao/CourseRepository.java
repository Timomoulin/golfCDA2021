package com.greta.golf.dao;

import com.greta.golf.model.Course;
import com.greta.golf.model.Golf;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long>
{
    List<Course> findAllByName(String name);
    List<Course> findAllByGolf(Golf golf);
}