package com.greta.golf.controller;

import com.greta.golf.dto.CourseDTO;
import com.greta.golf.dto.GolfDTO;
import com.greta.golf.dto.RoundDTO;
import com.greta.golf.dto.TournamentDTO;
import com.greta.golf.dto.controlForm.ControlRound;
import com.greta.golf.model.*;
import com.greta.golf.service.GolfService;
import com.greta.golf.service.PdfService;
import com.greta.golf.service.TournamentService;
import com.greta.golf.service.UserService;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfDocument;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.CsvToBeanFilter;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.print.Doc;
import javax.print.attribute.standard.Media;
import javax.validation.Valid;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
@Validated
@Controller
public class RefereeController {
    @Autowired
    ControlRound controlRound;
    @Autowired
    GolfService golfService;
    @Autowired
    UserService userService;
    @Autowired
    TournamentService tournamentService;
    @Autowired
    PdfService pdfService;
    /**
     * Route de la liste de golfs
     * @param model
     * @return listeGolf.html
     */
    @RequestMapping("/Arbitre/lesGolfs")
    public String routeGolf(Model model)
    {
        //this.golfService.populateGolf();
        List<Golf> lesGolfs=this.golfService.getAllGolf();
        System.out.println(lesGolfs);
        model.addAttribute("lesGolfs2",lesGolfs);
        return "listeGolf";
    }

    @RequestMapping("Arbitre/lesTours/{idTournois}")
    public String routeRound (Model model,@PathVariable long idTournois)
    {
        Tournament tournament= tournamentService.getTournamentByID(idTournois);
        model.addAttribute("tournois",tournament);
        return "listeRound";
    }

    /**
     * Route du formulaire d'ajout d'un golf
     * @param model
     * @return formAddGolf.html avec un objet golfDTO dans le model
     */
    @RequestMapping("/Arbritre/formAddGolf")
    public String routeFormAddGolf(Model model)
    {
        GolfDTO golfDTO=new GolfDTO();
        model.addAttribute("golfDTO",new GolfDTO());

        return "formAddGolf";
    }

    /*@RequestMapping( value = "/Arbitre/handleAjoutGolf",method = RequestMethod.POST)
    public String routeHandleAjoutGolf(Model model, @RequestParam String name,@RequestParam String gps)
    {
        golfService.addGolf(name,gps);
        return "redirect:/Arbitre/lesGolfs";
    }*/

    /**
     * Route qui recupère le formulaire d'ajout d'un golf
     * @param model
     * @param golfDTO Les données du formulaire
     * @return La route lesGolfs
     */
    @RequestMapping( value = "/Arbitre/handleAjoutGolf",method = RequestMethod.POST)
    public String routeHandleAjoutGolf(Model model, @ModelAttribute GolfDTO golfDTO)
    {
        System.out.println(golfDTO.getName());
        System.out.println(golfDTO.getCoorGPS());
        golfService.addGolf(golfDTO);
        return "redirect:/Arbitre/lesGolfs";

    }

    /**
     * Route du formulaire de modification d'un golf
     * @param model
     * @param idGolf
     * @return
     */
    @RequestMapping ("/Abritre/formUpdateGolf/{idGolf}")
    public String routeFormUpdateGolf(Model model, @PathVariable Long idGolf)
    {
        //@TODO modif DTO
        Golf leGolf=golfService.getGolfById(idGolf);
        model.addAttribute("leGolf",leGolf);
        return "formUpdateGolf";
    }

    @RequestMapping(value = "/Arbitre/handleUpdateGolf")
    public String routeHandleModificationGolf (Model model, @RequestParam Long idGolf, @RequestParam String name, @RequestParam String gps)
    {
        //@TODO modif DTO
        golfService.updateGolf(idGolf,name,gps);
        return "redirect:/Arbitre/lesGolfs";
    }

    @RequestMapping(value = "/Admin/Inscription")
    public String routeFormulaireInscription(Model model)
    {
        //@TODO modif DTO
        return "formInscription";
    }

    @RequestMapping(value = "/Admin/handleInscription")
    public String routeHandleInscription (Model model, @RequestParam String email,@RequestParam String nom, @RequestParam String mdp1,@RequestParam String mdp2)
    {
        //@TODO modif DTO
        System.out.println("Test CONTROLLEUR");
        System.out.println(mdp1.equals(mdp2));
        if(mdp1.equals(mdp2))
        {
            this.userService.saveUser(nom,email,mdp1);
        }

        return "redirect:/Admin/Inscription";
    }

    @GetMapping(value = "/Arbitre/formUpdateCourse/{idCourse}")
    public String routeFormUpdateCourse (Model model, @PathVariable Long idCourse){
        Course course= this.golfService.getCourseById(idCourse);
        CourseDTO courseDTO= new CourseDTO();
        courseDTO.setGolfId(course.getGolf().getId());
        courseDTO.setId(course.getId());
        courseDTO.setName(course.getName());
        courseDTO.setHoles(course.getHoles());
        model.addAttribute("formCourse",courseDTO);
        return "formUpdateCourse";

    }

    @GetMapping(value = "/Arbitre/formAddCourse/{idGolf}")
    public String routeFormAddCourse (Model model, @PathVariable Long idGolf)
    {
        CourseDTO courseDTO= new CourseDTO();
        courseDTO.setGolfId(idGolf);
        List<Hole> holes= new ArrayList<Hole>();
        for(int i=0;i<18;i++)
        {
            holes.add(new Hole(i+1));
        }
        courseDTO.setHoles(holes);
        model.addAttribute("formCourse",courseDTO);
        return"formAddCourse";
    }

    @PostMapping(value = "/Arbitre/handleAddForm")
    public String routeHandleAddForm(Model model,CourseDTO courseDTO)
    {
        golfService.saveCourse(courseDTO);
        return "redirect:/Arbitre/lesGolfs";

    }

    @GetMapping(value = "/Arbitre/formAddTourney/{idCourse}")
    public String routeFormAddTourney (Model model, @PathVariable Long idCourse)
    {
        TournamentDTO tournamentDTO= new TournamentDTO();
        tournamentDTO.setCourseId(idCourse);
        model.addAttribute("tournamentDTO",tournamentDTO);
        return "formAddTourney";
    }

    @GetMapping(value = "/Arbitre/formUpdateTourney/{idTournois}")
    public String routeFormUpdateTourney (Model model,@PathVariable Long idTournois){
        Tournament tournament= tournamentService.getTournamentByID(idTournois);
        TournamentDTO tournamentDTO = new TournamentDTO(tournament);
        model.addAttribute("tournamentDTO",tournamentDTO);
        return "formUpdateTourney";
    }

    @PostMapping(value = "/Abitre/handleAddTourney")
    public String handleAddTourney (Model model,@ModelAttribute TournamentDTO tournamentDTO) throws ParseException {
        tournamentService.saveTournament(tournamentDTO);
        return "redirect:/Arbitre/lesGolfs";
    }

    @RequestMapping(value = "/Arbitre/formAddRound/{idTournois}")
    public  String routeFormAddRound (Model model,@PathVariable long idTournois)
    {
        RoundDTO roundDTO= new RoundDTO();
        roundDTO.setIdTournament(idTournois);
        List<Attunement> attunements = new ArrayList<Attunement>();
        Course course= tournamentService.getTournamentByID(idTournois).getCourse();
        for (Hole hole: course.getHoles()
             ) {
            attunements.add(new Attunement(hole));
        }
        roundDTO.setAttunements(attunements);
        model.addAttribute("roundDTO",roundDTO);
        return "formAddRound";
    }

    @PostMapping(value = "/Arbitre/handleAddRound")
    public String handleAddRound (@Valid @ModelAttribute RoundDTO roundDTO,BindingResult result)
    {
       controlRound.validate(roundDTO,result);
        if(result.hasErrors())
        {
            System.out.println("Alerte rouge");
            return "redirect:/Arbitre/formAddRound/"+roundDTO.getIdTournament();
        }

            Document pdf = new Document();
            try (Reader reader = new BufferedReader(new InputStreamReader(roundDTO.getFile().getInputStream(), "UTF-16"))) {

                // create csv bean reader
                CsvToBean<Player> csvToBean = new CsvToBeanBuilder(reader)
                        .withType(Player.class)
                        .withIgnoreLeadingWhiteSpace(true)
                        .withIgnoreQuotations(true)
                        .withSkipLines(1)
                        .build();

                List<Player> players = csvToBean.parse();
                String numEquipe = "";
                String heure = "";
                List<Team> teams = new ArrayList<Team>();
                Team team = new Team();
                for (Player player : players) {
                    if (!player.getNumTeam().equals("")) {
                        numEquipe = player.getNumTeam();
                        heure = player.getStart();
                        team = new Team(numEquipe, heure, new ArrayList<Player>());
                        teams.add(team);
                    }
                    player.setNumTeam(numEquipe);
                    player.setStart(heure);
                    team.getPlayers().add(player);
                }
                Round round = tournamentService.saveRound(roundDTO);

                for (Team aTeam : teams
                ) {
                    tournamentService.genrateRate(aTeam, round);
                }
                pdf = pdfService.genaratePDF(teams, round);

            } catch (Exception ex) {

                System.out.println("An error occurred while processing the CSV file");
                System.out.println(ex.getMessage());
                System.out.println(ex.getCause());
                ex.printStackTrace();

            }

            return "redirect:/Arbitre/lesGolfs";
        }

}
