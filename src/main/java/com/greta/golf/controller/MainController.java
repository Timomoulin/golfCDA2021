package com.greta.golf.controller;

import com.greta.golf.dto.CourseDTO;
import com.greta.golf.dto.GolfDTO;
import com.greta.golf.model.Course;
import com.greta.golf.model.Golf;
import com.greta.golf.model.Hole;
import com.greta.golf.model.User;
import com.greta.golf.service.GolfService;
import com.greta.golf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {

    @Autowired
    GolfService golfService;
    @Autowired
    UserService userService;

    /**
     * Route de la page principale
     * @return home.html
     */
    @RequestMapping("/")
    public String routeHome() {

        return "home";
    }






}
