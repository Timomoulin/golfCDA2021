package com.greta.golf.model;

import java.util.Date;

public class Rate {
    private Attunement attunement;
    private Team team;
    private Date date;

    public Rate() {
    }

    public Rate(Attunement attunement, Date date) {
        this.attunement = attunement;
        this.date = date;
    }

    public Rate(Attunement attunement, Team team, Date date) {
        this.attunement = attunement;
        this.team = team;
        this.date = date;
    }

    public Attunement getAttunement() {
        return attunement;
    }

    public void setAttunement(Attunement attunement) {
        this.attunement = attunement;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
