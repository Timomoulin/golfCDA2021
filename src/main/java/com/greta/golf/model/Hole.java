package com.greta.golf.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
@Entity
public class Hole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column( nullable = false)
    private int num;
    private String name;
    @Column( nullable = false)
    private int par;
    @ManyToOne
    private Course course;
    @OneToMany(mappedBy = "hole")
    private List<Attunement> attunements;

    public Hole(long id, int num, String name, int par) {
        this.id = id;
        this.num = num;
        this.name = name;
        this.par = par;
    }
    public  Hole(int unNum)
    {
        this.num=unNum;
    }
    public Hole(long id, int num, String name, int par, Course course) {
        this.id = id;
        this.num = num;
        this.name = name;
        this.par = par;
        this.course = course;
    }

    public Hole(int num, String name, int par, Course course) {
        this.num = num;
        this.name = name;
        this.par = par;
        this.course = course;
    }

    public Hole() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPar() {
        return par;
    }

    public void setPar(int par) {
        this.par = par;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<Attunement> getAttunements() {
        return attunements;
    }

    public void setAttunements(List<Attunement> attunements) {
        this.attunements = attunements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hole trou = (Hole) o;
        return id == trou.id && num == trou.num && par == trou.par && Objects.equals(name, trou.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, num, name, par);
    }
}
