package com.greta.golf.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Golf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id ;
    @Column( nullable = false)
    private String name;
    private String coorGPS;
    @OneToMany(mappedBy = "golf")
    private List<Course> courses;


    public Golf(long id, String nom, String coorGPS) {
        this.id = id;
        this.name = nom;
        this.coorGPS = coorGPS;
    }

    public Golf(String name, String coorGPS) {
        this.name = name;
        this.coorGPS = coorGPS;
    }

    public Golf() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoorGPS() {
        return coorGPS;
    }

    public void setCoorGPS(String coorGPS) {
        this.coorGPS = coorGPS;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Golf golf = (Golf) o;
        return id == golf.id && name.equals(golf.name) && Objects.equals(coorGPS, golf.coorGPS) && Objects.equals(courses, golf.courses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, coorGPS);
    }
}
