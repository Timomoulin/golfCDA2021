package com.greta.golf.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Attunement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private int extraTime;
    @ManyToOne
    private Hole hole;
    @ManyToOne
    private Round round;

    public Attunement() {
    }

    public Attunement(Hole hole)
    {
        this.hole=hole;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getExtraTime() {
        return extraTime;
    }


    public void setExtraTime(int extraTime) {
        this.extraTime = extraTime;
    }

    public Hole getHole() {
        return hole;
    }

    public void setHole(Hole hole) {
        this.hole = hole;
    }

    public Round getRound() {
        return round;
    }

    public void setRound(Round round) {
        this.round = round;
    }
}
