package com.greta.golf.model;

import org.hibernate.annotations.GeneratorType;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Round {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private Date date;
    @Basic
    @Column( nullable = true)
    private String pdf;
    @ManyToOne
    private Tournament tournament;
    @OneToMany (mappedBy = "round")
    private List<Attunement> attunements;

    public Round() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public Tournament getTournament() {
        return tournament;
    }

    public void setTournament(Tournament tournament) {
        this.tournament = tournament;
    }

    public List<Attunement> getAttunements() {
        return attunements;
    }

    public void setAttunements(List<Attunement> attunements) {
        this.attunements = attunements;
    }

}
