package com.greta.golf.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_user;
    private String nom;
    @Column(unique = true)
    private String email;
    private String password;
    @ManyToMany
    @JoinTable(name="user_role",
            joinColumns={@JoinColumn(name="id_user")},
            inverseJoinColumns={@JoinColumn(name="id_role")})
    private List<Role> roles;
    //TODO tournois


    public User(long id_user, String nom, String email) {
        this.id_user = id_user;
        this.nom = nom;
        this.email = email;
    }

    public User(long id_user, String nom, String email, List<Role> roles) {
        this.id_user = id_user;
        this.nom = nom;
        this.email = email;
        this.roles = roles;
    }

    public User() {

    }

    public long getId_user() {
        return id_user;
    }

    public void setId_user(long id) {
        this.id_user = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id_user == user.id_user && Objects.equals(nom, user.nom) && email.equals(user.email) && roles.equals(user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_user, nom, email, roles);
    }
}
