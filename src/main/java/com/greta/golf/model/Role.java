package com.greta.golf.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_role;
    private String name;
    @ManyToMany(mappedBy="roles")
    private List<User> users;

    public Role(long id_role, String name, List<User> users) {
        this.id_role = id_role;
        this.name = name;
        this.users = users;
    }

    public Role() {

    }

    public long getId_role() {
        return id_role;
    }

    public void setId_role(long id) {
        this.id_role = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id_role == role.id_role && name.equals(role.name) && Objects.equals(users, role.users);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id_role, name, users);
    }
}
