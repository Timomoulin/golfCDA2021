package com.greta.golf.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column( nullable = false)
    private String name;
    @ManyToOne
    private Golf golf;
    @OneToMany(mappedBy = "course")
    private List<Hole> holes;
    //Todo Tournament
    @OneToMany(mappedBy = "course")
    private List<Tournament> tournaments;
    public Course(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Course(long id, String name, Golf golf, List<Hole> holes) {
        this.id = id;
        this.name = name;
        this.golf = golf;
        this.holes = holes;
    }

    public Course(String name, Golf golf) {
        this.name = name;
        this.golf = golf;
    }

    public Course() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Golf getGolf() {
        return golf;
    }

    public void setGolf(Golf golf) {
        this.golf = golf;
    }

    public List<Hole> getHoles() {
        return holes;
    }

    public void setHoles(List<Hole> holes) {
        this.holes = holes;
    }

    public List<Tournament> getTournaments() {
        return tournaments;
    }

    public void setTournaments(List<Tournament> tournaments) {
        this.tournaments = tournaments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Course course = (Course) o;
        return id == course.id && Objects.equals(name, course.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
