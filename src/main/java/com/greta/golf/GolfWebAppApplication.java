package com.greta.golf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GolfWebAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(GolfWebAppApplication.class, args);
    }

}
