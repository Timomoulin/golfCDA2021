package com.greta.golf.service;

import com.greta.golf.dao.AttunementRepository;
import com.greta.golf.dao.CourseRepository;
import com.greta.golf.dao.RoundRepository;
import com.greta.golf.dao.TournamentRepository;
import com.greta.golf.dto.RoundDTO;
import com.greta.golf.dto.TournamentDTO;
import com.greta.golf.model.*;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

@Service
public class TournamentService {
    @Autowired
    private TournamentRepository tournamentRepository;
    @Autowired
    private RoundRepository roundRepository;
    @Autowired
    private AttunementRepository attunementRepository;
    @Autowired
    private CourseRepository courseRepository;

    public Tournament getTournamentByID(long id)
    {
        return tournamentRepository.findById(id).get();
    }

    public void saveTournament (TournamentDTO tournamentDTO) throws ParseException {
        Tournament tournament= new Tournament();
        Course course = courseRepository.findById(tournamentDTO.getCourseId()).get();
        tournament.setCourse(course);
        tournament.setId(tournamentDTO.getId());
        tournament.setName(tournamentDTO.getName());
        tournament.setComment(tournamentDTO.getComment());
        //SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        //tournament.setDate(sdf.parse(tournamentDTO.getDate()));
        if(null!=tournamentDTO.getDate()) {
            tournament.setDate(tournamentDTO.getDate());

        }
        tournamentRepository.save(tournament);
    }

    public Round saveRound (RoundDTO roundDTO)
    {
        Round round= new Round();
        round.setId(roundDTO.getId());
        round.setDate(roundDTO.getDate());
        round.setTournament(tournamentRepository.findById(roundDTO.getIdTournament()).get());
        for (Attunement attunement:roundDTO.getAttunements()
             ) {
        attunement.setRound(round);
        }
        round.setAttunements(roundDTO.getAttunements());
        roundRepository.save(round);
        attunementRepository.saveAll(round.getAttunements());
        return round;
    }

    public List<Rate> genrateRate(Team team,Round round) throws ParseException {
        List<Rate> rates= new ArrayList<Rate>();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("HH'h'mm");
        Date jour= round.getDate();
        Calendar cl = Calendar. getInstance();
        cl.setTime(jour);
        cl.getTimeZone().getDisplayName();
        for (Attunement attunement:round.getAttunements()
             ) {
            Date d0;
            int tempsStandar;
            if(attunement.getHole().getPar()==3)
            {
                tempsStandar=11;
            }
            else if(attunement.getHole().getPar()==4)
            {
                tempsStandar=14;
            }
            else {
                tempsStandar=17;
            }
            if(rates.size()==0)
            {
                d0=jour;
                Date time=simpleDateFormat.parse(team.getStart());
                cl.add(Calendar.HOUR,time.getHours());
                cl.add(Calendar.MINUTE,time.getMinutes()+attunement.getExtraTime()+tempsStandar);
            }
            else{
                d0=rates.get(rates.size()-1).getDate();
                cl.add(Calendar.MINUTE,attunement.getExtraTime()+tempsStandar);

            }
            Rate rate = new Rate(attunement,cl.getTime());
            rates.add(rate);
        }
        team.setRates(rates);
        return rates;
    }



//https://loizenai.com/upload-read-csv-spring-boot/
//https://grokonez.com/spring-framework/spring-boot/csv-file-upload-download-using-apache-commons-csv-springboot-restapis-spring-jpa-thymeleaf-to-mysql
    //https://www.baeldung.com/java-pdf-creation
}
