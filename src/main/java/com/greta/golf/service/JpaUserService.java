package com.greta.golf.service;

import com.greta.golf.dao.UserRepository;
import com.greta.golf.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class JpaUserService {
    @Autowired
    private UserRepository userDao;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void setUserDao(UserRepository userDao) {
        this.userDao = userDao;
    }

    public JpaUserService() {
        this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
    }

    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    public User findByEmail(String email){
        return userDao.findByEmail(email);
    }

//    public User findByUserName(String userName) {
//        return userDao.findByName(userName);
//    }
}
