package com.greta.golf.service;

import com.greta.golf.dao.UserRepository;
import com.greta.golf.model.User;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class UserService {
    UserRepository userRepository;
    JpaUserService jpaUserService;

    @Autowired
    public  UserService (UserRepository userRep,JpaUserService jpaUserRep)
    {
    this.userRepository=userRep;
    this.jpaUserService=jpaUserRep;
    }

    public void saveUser(String nom,String email,String mdp)
    {
        User user=new  User();
        user.setNom(Jsoup.parse(nom).text());
        user.setEmail(Jsoup.parse(email).text());
        user.setPassword(Jsoup.parse(mdp).text());
        this.jpaUserService.save(user);
        Date date= new Date();

    }
}
