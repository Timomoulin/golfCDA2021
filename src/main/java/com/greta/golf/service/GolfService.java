package com.greta.golf.service;

import com.greta.golf.dao.CourseRepository;
import com.greta.golf.dao.GolfRepository;
import com.greta.golf.dao.HoleRepository;
import com.greta.golf.dto.CourseDTO;
import com.greta.golf.dto.GolfDTO;
import com.greta.golf.model.Course;
import com.greta.golf.model.Golf;
import com.greta.golf.model.Hole;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GolfService {
    GolfRepository golfRepository;
    CourseRepository courseRepository;
    HoleRepository holeRepository;

    @Autowired
    public GolfService(GolfRepository golfRepository, CourseRepository courseRepository, HoleRepository holeRepository)
    {
        this.golfRepository=golfRepository;
        this.courseRepository=courseRepository;
        this.holeRepository=holeRepository;
    }


    public List<Golf> getAllGolf(){
        return this.golfRepository.findAll();
    }

    public Golf getGolfById(Long idGolf)
    {
        String stringIdGolfParser=Jsoup.parse(idGolf.toString()).text();
        Long idGolfParser=Long.parseLong(stringIdGolfParser) ;
        Golf leGolf=golfRepository.findById(idGolfParser).get();
        return leGolf;

    }

    public Golf addGolf(GolfDTO golfDTO)
    {
        //TODO JSOUP et verification de nomGolf et gps
        String nomGolfParser=golfDTO.getName();
        String gpsParser=golfDTO.getCoorGPS();
        Golf leGolf=new Golf(nomGolfParser,gpsParser);
        golfRepository.save(leGolf);
        return leGolf;
    }

    public void updateGolf(Long id,String nomGolf, String gps)
    {
        Long idParser= Long.parseLong( Jsoup.parse(id.toString()).text());
        String nomParser= Jsoup.parse(nomGolf).text();
        String gpsParser=Jsoup.parse(gps).text();
        Golf leGolf=this.getGolfById(idParser);
        leGolf.setName(nomParser);
        leGolf.setCoorGPS(gpsParser);
        golfRepository.save(leGolf);
    }

    public void  deleteGolf(Long idGolf)
    {
        Long idParser= Long.parseLong(Jsoup.parse(idGolf.toString()).text());
        golfRepository.deleteById(idParser);
    }

    public void populateGolf(){
        Golf golf1= new Golf("Chateau Roux","lat:125,long:195");
        Golf golf2= new Golf("Chateau Blond","lat:150,long:200");

        Course course1= new Course("Principale",golf1);
        Course course2= new Course("Parcours Blond",golf2);

        List<Course> listeCourseGolf1= new ArrayList<>();
        listeCourseGolf1.add(course1);
        golf1.setCourses(listeCourseGolf1);

        List<Course> listeCourseGolf2= new ArrayList<>();
        listeCourseGolf2.add(course2);
        golf2.setCourses(listeCourseGolf2);

        this.golfRepository.save(golf1);
        this.golfRepository.save(golf2);
    }

    public Course getCourseById(Long idCourse)
    {
        return  courseRepository.findById(idCourse).orElse(new Course());
    }

    public void saveCourse(CourseDTO courseDTO)
    {
        Course course = new Course();
        course.setId(courseDTO.getId());
        course.setName(courseDTO.getName());
        course.setGolf(golfRepository.findById(courseDTO.getGolfId()).get());
        for(Hole hole:courseDTO.getHoles())
        {
            hole.setCourse(course);
        }
        course.setHoles(courseDTO.getHoles());
        courseRepository.save(course);
        holeRepository.saveAll(course.getHoles());

    }
}
