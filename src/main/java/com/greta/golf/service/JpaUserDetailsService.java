package com.greta.golf.service;

import com.greta.golf.dao.UserRepository;
import com.greta.golf.model.Role;
import com.greta.golf.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class JpaUserDetailsService implements UserDetailsService {
    private UserRepository userDao;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public void setUserDao(UserRepository userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        System.out.println("==== LOAD USER NAME ===");
        User user = userDao.findByEmail(email);
        log.info("Recherche utilisateur: " + email);
        if (user == null) {
            throw new UsernameNotFoundException("Utilisateur introuvable : |" + email + "|");
        }
        Set<GrantedAuthority> authorities = new HashSet<>();
        List<Role> roles=user.getRoles();
        for (Role role:roles
             ) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(),
                user.getPassword(),
                authorities);
    }

}
