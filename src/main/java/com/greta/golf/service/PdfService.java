package com.greta.golf.service;

import com.greta.golf.model.Player;
import com.greta.golf.model.Rate;
import com.greta.golf.model.Round;
import com.greta.golf.model.Team;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Stream;

@Service
public class PdfService {
    String path;
    public int savePdf(Round r, InputStream is){
        r.setPdf(save("f", "affiches", is));
        return 0;
    }

    private String save(String prefix, String subPath, InputStream fi){
        String fileName = "";
        try(DirectoryStream<Path> dir = Files.newDirectoryStream(Paths.get(path+"/"+subPath),prefix+"*")){

            for (Path file: dir
            ) {
                if(fileName.compareTo(file.getFileName().toString())<0){
                    fileName = file.getFileName().toString();
                }
            }
            String numStr = fileName.substring(1, fileName.indexOf(".pdf"));

            Integer num = Integer.parseInt(numStr);

            numStr = String.format("%04d",num+1);

            fileName = prefix+numStr+".pdf";

            String filePath = path+"/"+subPath+"/"+fileName;

            Files.copy(fi, new File(filePath).toPath());

        }catch (IOException ioe){
            System.out.println("Erreur sur nom d'image : "+ioe.getMessage());
        }


        return fileName;
    }

    public InputStream retreivePdf(String fileName){
        return retreiveFile("pdf", fileName);
    }

    private InputStream retreiveFile(String subPath, String fileName){
        InputStream is = null;
        try {
            is = new FileInputStream(path+"/"+subPath+"/"+fileName);
        } catch (FileNotFoundException fnfe) {
            System.out.println("Erreur récupération du fichier "+fileName+" : "+fnfe.getMessage());
        }
        return is;
    }

    public Document genaratePDF(List<Team> teams, Round round) throws FileNotFoundException, DocumentException {
        Document document = new Document(PageSize.A2.rotate());
        PdfWriter.getInstance(document, new FileOutputStream("src/main/resources/static/pdf/cadence"+round.getId()+".pdf"));
        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Chunk chunk = new Chunk(round.getTournament().getName(), font);

        PdfPTable table = new PdfPTable(21);
        table.setWidthPercentage(100);
        addTableHeader(table);
        for (Team t:teams
        ) {
            addRows(table,t);
        }
        //addCustomRows(table);
        document.add(chunk);
        document.add(table);
        document.close();
        return document;
    }

    private void addTableHeader(PdfPTable table) {
        Stream.of("#", "Départ", "Joueurs","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addRows(PdfPTable table,Team team) {
        table.addCell(team.getNum());
        table.addCell(team.getStart());
        PdfPTable tableNoms=new PdfPTable(1);
        for (Player p:team.getPlayers())
        {
            tableNoms.addCell(p.getName());
        }
        table.addCell(tableNoms);
        DateFormat dateFormat= new SimpleDateFormat("hh:mm");
        for (Rate r:team.getRates()
        ) {
            table.addCell(dateFormat.format(r.getDate()));
        }

    }
}
