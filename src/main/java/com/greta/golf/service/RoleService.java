package com.greta.golf.service;

import com.greta.golf.dao.RoleRepository;
import com.greta.golf.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    RoleRepository roleRepository;

    @Autowired
    public RoleService (RoleRepository roleRepository)
    {
    this.roleRepository=roleRepository;
    }

    public Role getRoleById(long idRole)
    {
        return this.roleRepository.findById(idRole).get();
    }

    public Role getRoleByName(String unNom)
    {
        return this.roleRepository.findRoleByName(unNom);
    }
}
